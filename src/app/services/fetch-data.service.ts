import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FetchDataService {
  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) { }

  getProfile(): Observable<any> {
    return this.http.get(`${this.baseUrl}/vrideapi/v1/sales_users/list/offset/0/limit/10/token/5eee167482786`);
  }
  sendOTP(mobile): Observable<any> {
    return this.http.get(`${this.baseUrl}/vrideapi/v1/auth/getOTP/mobile/${mobile}`);
  }
  verifyOTP(mobile,otp,customerName): Observable<any> {
    return this.http.get(`${this.baseUrl}/vrideapi/v1/auth/verify/mobile/${mobile}/otp/${otp}/name/${customerName}`);
  }
  search(x): Observable<any>{
    return this.http.get(`${this.baseUrl}/vrideapi/v1/sales_users/list/offset/0/limit/10/token/5eee167482786/search_tag/${x}`);
  }
  availableSlots(drid,cdate): Observable<any>{
    return this.http.get(`${this.baseUrl}/vrideapi/v1/get/doctor_available/slots/token/5eee167482786/id/${drid}/date/${cdate}`);
  }
  scheduleCall(data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/vrideapi/v1/post/schedule_call/token/5eee167482786`, data);
  }
  paymentSuccess(success:any): Observable<any> {
    return this.http.post(`${this.baseUrl}/vrideapi/v1/post/payment/success/token/5eee167482786`, success)
  }
  customerSchedule(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/vrideapi/v1/get/customer_schedule/token/5eee167482786/customer_id/${id}`)
  }
  getSpecialitites(s): Observable<any> {
    return this.http.get(`${this.baseUrl}/vrideapi/v1/sales_users/list/offset/0/limit/10/token/5eee167482786/specialitites/${s}`)
  }
  getDoctor(d): Observable<any> {
    return this.http.get(`${this.baseUrl}/vrideapi/v1/sales_users/list/offset/0/limit/10/token/5eee167482786/doctor/${d}`)
  }
  getClinic(c): Observable<any> {
    return this.http.get(`${this.baseUrl}/vrideapi/v1/sales_users/list/offset/0/limit/10/token/5eee167482786/clinic/${c}`)
  }
}
