import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
showlogin=true;
  constructor(private router: Router) { }

  ngOnInit(): void {
    if(localStorage.getItem('uid')){
      this.showlogin = false;
    }
  }
  logout(){
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
