import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path:'', redirectTo:'home', pathMatch:'prefix'},
  {path:'login', loadChildren:()=>import('./core-components/login/login.module').then(m=>m.LoginModule)},
  {path:'home', loadChildren: ()=>import('./core-components/home/home.module').then(m=>m.HomeModule)},
  { path: 'details/:id', loadChildren: () => import('./core-components/details/details.module').then(m => m.DetailsModule) },
  {path:'appointments', loadChildren:() => import('./core-components/appointements/appointements.module').then(m=>m.AppointementsModule)},
  { path: 'slots/:id/:name', loadChildren: () => import('./core-components/slots/slots.module').then(m => m.SlotsModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
