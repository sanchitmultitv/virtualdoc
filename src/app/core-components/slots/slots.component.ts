import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';
@Component({
  selector: 'app-slots',
  templateUrl: './slots.component.html',
  styleUrls: ['./slots.component.scss']
})
export class SlotsComponent implements OnInit {
  Drid: any;
  Drname: any;
  cdate: any;
  cdate2:any;
  cdate3:any;
  drDetailed: any;
  drDetails: any;
  drDetailsEve: any;
  drDetailsMor: any;
  drDetailsNoon: any;
  constructor(private route: ActivatedRoute, private fd: FetchDataService) { }

  ngOnInit(): void {

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    this.cdate = yyyy + '-' + mm + '-' + dd;
    
    const tomorrow =  new Date(today.setDate(today.getDate() + 1));
    var dd2 = String(tomorrow.getDate()).padStart(2, '0');
    var mm2 = String(tomorrow.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy2 = tomorrow.getFullYear();
    this.cdate2 = yyyy2 + '-' + mm2 + '-' + dd2;

    const dayaftertomorrow =  new Date(tomorrow.setDate(tomorrow.getDate() + 1));
    var dd3 = String(dayaftertomorrow.getDate()).padStart(2, '0');
    var mm3 = String(dayaftertomorrow.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy3 = dayaftertomorrow.getFullYear();
    this.cdate3 = yyyy3 + '-' + mm3 + '-' + dd3;
    
    this.route.paramMap.subscribe(params => {
      this.Drid = params.get('id');
      this.fd.availableSlots(this.Drid, this.cdate).subscribe((res => {
        console.log(res);
         this.drDetails= res.result;
         this.drDetailsMor = this.drDetails.morning
         this.drDetailsNoon = this.drDetails.afternoon
         this.drDetailsEve = this.drDetails.evening
        
      }));

      this.Drname = params.get('name');
      this.fd.search(this.Drname).subscribe((res => {
        console.log(res);
        this.drDetailed= res.result[0];
      }))
    });
  }
  dated(data){
    this.route.paramMap.subscribe(params => {
    this.Drid = params.get('id');
      this.fd.availableSlots(this.Drid, data).subscribe((res => {
        console.log(res);
         this.drDetails= res.result;
         this.drDetailsMor = this.drDetails.morning
         this.drDetailsNoon = this.drDetails.afternoon
         this.drDetailsEve = this.drDetails.evening
      }));
    });
  }
}
