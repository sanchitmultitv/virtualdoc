import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
doctros:any;
serachval ='';
  constructor(private fd : FetchDataService) { }

  ngOnInit(): void {
    this.getDoctors();
  }
  myFun(value){
   // alert(value.target.value);
    this.serachval = value.target.value;
    this.fd.getDoctor(this.serachval).subscribe(res=>{
      this.doctros = res.result;
    });
    if(this.serachval == ''){
      this.fd.getProfile().subscribe(res=>{
        //console.log(res.result);
        this.doctros = res.result;
      })
    }
  }
  getDoctors(){
    this.fd.getProfile().subscribe(res=>{
      //console.log(res.result);
      this.doctros = res.result;
    })
  }
}
