import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  id: any;
  drList : any;
  constructor(private route: ActivatedRoute,private fd : FetchDataService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.fd.search(this.id).subscribe((res=>{
        console.log(res);
        this.drList = res.result[0];
        console.log(this.drList)
      }))
    });
  }
}