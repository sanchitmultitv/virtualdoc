import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppointementsComponent } from './appointements.component';

const routes: Routes = [
  {path:'', component: AppointementsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppointementsRoutingModule { }
