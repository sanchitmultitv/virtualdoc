import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppointementsRoutingModule } from './appointements-routing.module';
import { AppointementsComponent } from './appointements.component';


@NgModule({
  declarations: [AppointementsComponent],
  imports: [
    CommonModule,
    AppointementsRoutingModule
  ]
})
export class AppointementsModule { }
