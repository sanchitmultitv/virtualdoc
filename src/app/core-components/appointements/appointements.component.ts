import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-appointements',
  templateUrl: './appointements.component.html',
  styleUrls: ['./appointements.component.scss']
})
export class AppointementsComponent implements OnInit {
myappoints:any;
  constructor(private fd : FetchDataService) { }

  ngOnInit(): void {
    this.getAppointments();
  }
getAppointments(){
  let id = localStorage.getItem('uid');
  this.fd.customerSchedule(id).subscribe(res=>{
    console.log(res);
    this.myappoints = res.result;
  })
}
}
