import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import {FormGroup, FormControl} from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    name: new FormControl(''),
    mobile: new FormControl(''),
    otp: new FormControl(''),
  });
  constructor(private fd:FetchDataService, private router: Router) { }
  otp=false;
  ngOnInit(): void {
  }
  sendOtp(){
    this.fd.sendOTP(this.loginForm.value.mobile).subscribe(res=>{
      console.log(res);
      if(res.code == 1){
        this.otp = true;
      }
    })
  }
  verifyOtp(){
    this.fd.verifyOTP(this.loginForm.value.mobile,this.loginForm.value.otp, this.loginForm.value.name).subscribe(res=>{
      console.log(res);
      if(res.code == '1'){
        localStorage.setItem('roomid', res.Room_id);
        localStorage.setItem('uid', res.id);
        this.router.navigate(['/home']);
      }else{
        alert("Oops !! , Seems like you have Entered Wrong OTP ")
      }
    });
  }
}
